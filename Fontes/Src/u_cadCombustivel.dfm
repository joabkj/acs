inherited cadCombustivel: TcadCombustivel
  Tag = 2
  Caption = 'cadCombustivel'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnl_botoes: TPanel
    inherited btn_cancelar: TButton
      Left = 4
      ExplicitLeft = 4
    end
  end
  inherited pg_principal: TPageControl
    inherited tb_consulta: TTabSheet
      inherited pnl_consulta: TPanel
        inherited btn_pesquisar: TButton
          OnClick = btn_pesquisarClick
        end
      end
      inherited gdr_principal: TDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_COMBUSTIVEIS'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'DESCRI'#199#195'O'
            Width = 570
            Visible = True
          end>
      end
    end
    inherited tb_dados: TTabSheet
      inherited db_codigo: TDBEdit
        DataField = 'ID_COMBUSTIVEIS'
        DataSource = ds_dados
      end
      inherited db_nome: TDBEdit
        DataField = 'DESCRICAO'
        DataSource = ds_dados
      end
    end
  end
  inherited qy_dados: TFDQuery
    object qy_dadosID_COMBUSTIVEIS: TIntegerField
      FieldName = 'ID_COMBUSTIVEIS'
      Origin = 'ID_COMBUSTIVEIS'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qy_dadosDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 50
    end
  end
end
