inherited cadPostos: TcadPostos
  Tag = 1
  Caption = 'cadPostos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pg_principal: TPageControl
    ActivePage = tb_dados
    inherited tb_consulta: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 6
      ExplicitWidth = 676
      ExplicitHeight = 401
      inherited gdr_principal: TDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_POSTO'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'DESCRI'#199#195'O'
            Width = 339
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ'
            Width = 111
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE'
            Width = 104
            Visible = True
          end>
      end
    end
    inherited tb_dados: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 6
      ExplicitWidth = 676
      ExplicitHeight = 401
      object Label8: TLabel [2]
        Left = 351
        Top = 40
        Width = 25
        Height = 13
        Caption = 'CNPJ'
        FocusControl = db_cnpj
      end
      object Label9: TLabel [3]
        Left = 5
        Top = 82
        Width = 50
        Height = 13
        Caption = 'TELEFONE'
        FocusControl = db_telefone
      end
      inherited db_codigo: TDBEdit
        DataField = 'ID_POSTO'
        DataSource = ds_dados
      end
      inherited db_nome: TDBEdit
        DataField = 'DESCRICAO'
        DataSource = ds_dados
      end
      object db_cnpj: TDBEdit
        Left = 351
        Top = 56
        Width = 199
        Height = 22
        DataField = 'CNPJ'
        DataSource = ds_dados
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object db_telefone: TDBEdit
        Left = 5
        Top = 98
        Width = 140
        Height = 22
        DataField = 'TELEFONE'
        DataSource = ds_dados
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  inherited qy_dados: TFDQuery
    SQL.Strings = (
      'SELECT * FROM POSTO')
    object qy_dadosID_POSTO: TIntegerField
      FieldName = 'ID_POSTO'
      Origin = 'ID_POSTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qy_dadosDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 200
    end
    object qy_dadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'CNPJ'
      Required = True
      Size = 15
    end
    object qy_dadosTELEFONE: TWideStringField
      FieldName = 'TELEFONE'
      Origin = 'TELEFONE'
      EditMask = '(99)99999-9999;0;_'
      Size = 15
    end
  end
end
