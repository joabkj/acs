unit u_dmDados;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, u_connection;

type
  TDados = class(TDataModule)
    qy_postos: TFDQuery;
    ds_postos: TDataSource;
    qy_postosID_POSTO: TIntegerField;
    qy_postosDESCRICAO: TWideStringField;
    qy_postosCNPJ: TWideStringField;
    qy_postosTELEFONE: TWideStringField;
    qy_bombas: TFDQuery;
    ds_bombas: TDataSource;
    qy_bombasID_POSTO: TIntegerField;
    qy_bombasID_BOMBAS: TIntegerField;
    qy_bombasDESCRICAO: TWideStringField;
    qy_combustivel: TFDQuery;
    ds_combustivel: TDataSource;
    qy_combustivelID_BOMBAS: TIntegerField;
    qy_combustivelID_COMBUSTIVEIS: TIntegerField;
    qy_combustivelDESCRICAO: TWideStringField;
    qy_combustivelID_TANQUES: TIntegerField;
    qy_combustivelID_POSTO: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure refresh;
    procedure fechar;
    procedure Abrir;
  end;

var
  Dados: TDados;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDados.Abrir;
begin
  qy_postos.Open;
  qy_bombas.Open;
  qy_combustivel.Open;
end;

procedure TDados.DataModuleCreate(Sender: TObject);
begin
  qy_postos.Connection:= TConnection.GetInstance.Conexao;
  qy_bombas.Connection:= qy_postos.Connection;
  qy_combustivel.Connection:= qy_postos.Connection;
  Abrir;
end;

procedure TDados.fechar;
begin
  qy_postos.Close;
  qy_bombas.Close;
  qy_combustivel.Close;
end;

procedure TDados.refresh;
begin
  qy_postos.Refresh;
  qy_bombas.Refresh;
  qy_combustivel.Refresh;
end;

end.
