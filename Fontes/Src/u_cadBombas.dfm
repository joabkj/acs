inherited cadBombas: TcadBombas
  Tag = 4
  Caption = 'cadBombas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pg_principal: TPageControl
    inherited tb_consulta: TTabSheet
      inherited gdr_principal: TDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_BOMBAS'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'DESCRI'#199#195'O'
            Visible = True
          end>
      end
    end
    inherited tb_dados: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 6
      ExplicitWidth = 676
      ExplicitHeight = 401
      inherited db_codigo: TDBEdit
        DataField = 'ID_BOMBAS'
        DataSource = ds_dados
      end
      inherited db_nome: TDBEdit
        DataField = 'DESCRICAO'
        DataSource = ds_dados
      end
    end
  end
  inherited qy_dados: TFDQuery
    SQL.Strings = (
      'SELECT * FROM BOMBAS')
    object qy_dadosID_BOMBAS: TIntegerField
      FieldName = 'ID_BOMBAS'
      Origin = 'ID_BOMBAS'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qy_dadosDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 50
    end
  end
end
