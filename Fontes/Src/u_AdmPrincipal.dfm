object admPrincipal: TadmPrincipal
  Left = 0
  Top = 0
  Caption = 'Prova - Joab Maia '
  ClientHeight = 359
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 336
    Width = 757
    Height = 23
    Panels = <
      item
        Text = ' Data: '
        Width = 35
      end
      item
        Width = 200
      end
      item
        Text = 'Hora: '
        Width = 40
      end
      item
        Width = 55
      end
      item
        Text = 'Usu'#225'rio : ADMINISTRADOR'
        Width = 300
      end
      item
        Text = 'Servidor: LOCAL'
        Width = 150
      end
      item
        Text = 'Banco de dados: POSTO'
        Width = 50
      end>
  end
  object pnl_menu: TPanel
    Left = 0
    Top = 0
    Width = 757
    Height = 63
    Align = alTop
    TabOrder = 1
    object SpeedButton1: TSpeedButton
      Left = 150
      Top = 8
      Width = 138
      Height = 49
      Action = ac_abastecimento
    end
    object SpeedButton2: TSpeedButton
      Left = 6
      Top = 8
      Width = 138
      Height = 49
      Action = ac_postos
    end
  end
  object MainMenu1: TMainMenu
    Left = 440
    Top = 152
    object Cadastros1: TMenuItem
      Caption = 'Cadastros'
      object Postos1: TMenuItem
        Action = ac_postos
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Combustiveis1: TMenuItem
        Action = ac_combustiveis
      end
      object anques1: TMenuItem
        Action = ac_tanques
      end
      object Bombas1: TMenuItem
        Action = ac_bombas
      end
    end
    object Movimentao1: TMenuItem
      Caption = 'Movimenta'#231#227'o'
      object Abastecimento1: TMenuItem
        Action = ac_abastecimento
      end
    end
    object Relatrios1: TMenuItem
      Caption = 'Relat'#243'rios '
      object Geralporperodo1: TMenuItem
        Action = rel_abastecimento
      end
    end
    object Sobre1: TMenuItem
      Caption = 'Sobre'
      object Verso101: TMenuItem
        Caption = 'Vers'#227'o 1.0'
      end
    end
  end
  object tm_DataHora: TTimer
    OnTimer = tm_DataHoraTimer
    Left = 304
    Top = 88
  end
  object ac_formularios: TActionList
    Left = 144
    Top = 160
    object ac_postos: TAction
      Caption = 'Postos'
      OnExecute = ac_postosExecute
    end
    object ac_combustiveis: TAction
      Caption = 'Combustiveis'
      OnExecute = ac_combustiveisExecute
    end
    object ac_bombas: TAction
      Caption = 'Bombas'
      OnExecute = ac_bombasExecute
    end
    object ac_impostos: TAction
      Caption = 'ac_impostos'
    end
    object ac_abastecimento: TAction
      Caption = 'Abastecimento'
      OnExecute = ac_abastecimentoExecute
    end
    object ac_tanques: TAction
      Caption = 'Tanques'
      OnExecute = ac_tanquesExecute
    end
    object rel_abastecimento: TAction
      Caption = 'Abastecimento por per'#237'odo'
      OnExecute = rel_abastecimentoExecute
    end
  end
end
