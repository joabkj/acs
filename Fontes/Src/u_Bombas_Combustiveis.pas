unit u_Bombas_Combustiveis;

interface

uses u_PersistentObject,u_Atributo;

type
  [Tablename('TANQUES_BOMBAS')]
  TBombas_Combustiveis = class(TPersintentObject)
  private
    FID: Integer;
    FDescricao: String;
    FID_COMBUSTIVEL: Integer;
    FID_TANQUES: Integer;
    FCampo_FK : String;
  public
    [FieldName('ID_TANQUES_BOMBAS','ID_TANQUES',true,true,true)]
    property ID: Integer read FID write FID;
    property DESCRICAO: String read FDescricao write FDescricao;
    property ID_TANQUES: Integer read FID_TANQUES write FID_TANQUES;
    property ID_COMBUSTIVEL: Integer read FID_COMBUSTIVEL write FID_COMBUSTIVEL;
    property CAMPO_FK: String read FCampo_FK write FCampo_FK;
    procedure Load(const AValue: Integer; ACampoFK: String = ''); override;
  end;

implementation

{ TBombas_Combustiveis }

procedure TBombas_Combustiveis.Load(const AValue: Integer; ACampoFK: String = '');
begin
  FID       := AValue;
  FCampo_FK := ACampoFK;
  inherited GetID(FID);
end;

end.
