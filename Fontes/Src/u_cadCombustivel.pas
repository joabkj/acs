unit u_cadCombustivel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, u_cadBasico, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Actions, Vcl.ActnList,
  System.ImageList, Vcl.ImgList, Vcl.Menus, Vcl.Mask, Vcl.DBCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait;

type
  TcadCombustivel = class(TcadBasico)
    qy_dadosID_COMBUSTIVEIS: TIntegerField;
    qy_dadosDESCRICAO: TWideStringField;
    function valida: Boolean; override;
    function pesq_complementar: String; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  cadCombustivel: TcadCombustivel;

implementation

{$R *.dfm}

{ TcadCombustivel }

function TcadCombustivel.pesq_complementar: String;
begin
  result:= '';
end;

function TcadCombustivel.valida: Boolean;
begin
  result:= True;
  if trim(db_nome.Text) = '' then
  begin
    Showmessage('Nome e obrigatório.');
    result:= False;
    db_nome.SetFocus;
    exit;
  end;
end;

initialization
    RegisterClass(TcadCombustivel);


end.
