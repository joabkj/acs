object Dados: TDados
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 345
  Width = 311
  object qy_postos: TFDQuery
    SQL.Strings = (
      'SELECT * FROM POSTO')
    Left = 32
    Top = 16
    object qy_postosID_POSTO: TIntegerField
      FieldName = 'ID_POSTO'
      Origin = 'ID_POSTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qy_postosDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 200
    end
    object qy_postosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'CNPJ'
      Required = True
      Size = 15
    end
    object qy_postosTELEFONE: TWideStringField
      FieldName = 'TELEFONE'
      Origin = 'TELEFONE'
      Size = 15
    end
  end
  object ds_postos: TDataSource
    DataSet = qy_postos
    Left = 88
    Top = 16
  end
  object qy_bombas: TFDQuery
    IndexFieldNames = 'ID_POSTO'
    MasterSource = ds_postos
    MasterFields = 'ID_POSTO'
    SQL.Strings = (
      'SELECT DISTINCT T.ID_POSTO, B.ID_BOMBAS, B.DESCRICAO '
      'FROM BOMBAS B'
      'JOIN TANQUES_BOMBAS TB ON TB.ID_BOMBAS = B.ID_BOMBAS '
      'JOIN TANQUES T ON T.ID_TANQUES = TB.ID_TANQUES'
      ''
      ''
      '')
    Left = 32
    Top = 72
    object qy_bombasID_POSTO: TIntegerField
      FieldName = 'ID_POSTO'
      Origin = 'ID_POSTO'
      Required = True
    end
    object qy_bombasID_BOMBAS: TIntegerField
      FieldName = 'ID_BOMBAS'
      Origin = 'ID_BOMBAS'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qy_bombasDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 50
    end
  end
  object ds_bombas: TDataSource
    DataSet = qy_bombas
    Left = 88
    Top = 72
  end
  object qy_combustivel: TFDQuery
    IndexFieldNames = 'ID_BOMBAS;ID_POSTO'
    MasterSource = ds_bombas
    MasterFields = 'ID_BOMBAS;ID_POSTO'
    SQL.Strings = (
      
        'SELECT DISTINCT T.ID_TANQUES, T.ID_POSTO, TB.ID_BOMBAS, C.ID_COM' +
        'BUSTIVEIS, C.DESCRICAO'
      'FROM combustiveis C'
      'JOIN TANQUES T ON T.ID_COMBUSTIVEIS = C.ID_COMBUSTIVEIS '
      'JOIN TANQUES_BOMBAS TB ON TB.ID_TANQUES = T.ID_TANQUES')
    Left = 32
    Top = 128
    object qy_combustivelID_BOMBAS: TIntegerField
      FieldName = 'ID_BOMBAS'
      Origin = 'ID_BOMBAS'
      Required = True
    end
    object qy_combustivelID_COMBUSTIVEIS: TIntegerField
      FieldName = 'ID_COMBUSTIVEIS'
      Origin = 'ID_COMBUSTIVEIS'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qy_combustivelDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object qy_combustivelID_TANQUES: TIntegerField
      FieldName = 'ID_TANQUES'
      Origin = 'ID_TANQUES'
      Required = True
    end
    object qy_combustivelID_POSTO: TIntegerField
      FieldName = 'ID_POSTO'
      Origin = 'ID_POSTO'
      Required = True
    end
  end
  object ds_combustivel: TDataSource
    DataSet = qy_combustivel
    Left = 112
    Top = 128
  end
end
