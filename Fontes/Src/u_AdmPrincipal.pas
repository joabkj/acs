unit u_AdmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Menus, Vcl.ExtCtrls,
  System.Actions, Vcl.ActnList, Vcl.Buttons, u_connection, u_basRelatorio;

type
  TadmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    Movimentao1: TMenuItem;
    Relatrios1: TMenuItem;
    Sobre1: TMenuItem;
    Postos1: TMenuItem;
    Combustiveis1: TMenuItem;
    N1: TMenuItem;
    Bombas1: TMenuItem;
    Abastecimento1: TMenuItem;
    Geralporperodo1: TMenuItem;
    Verso101: TMenuItem;
    StatusBar1: TStatusBar;
    tm_DataHora: TTimer;
    ac_formularios: TActionList;
    ac_postos: TAction;
    ac_combustiveis: TAction;
    ac_bombas: TAction;
    ac_impostos: TAction;
    pnl_menu: TPanel;
    SpeedButton1: TSpeedButton;
    ac_abastecimento: TAction;
    SpeedButton2: TSpeedButton;
    ac_tanques: TAction;
    anques1: TMenuItem;
    rel_abastecimento: TAction;
    procedure tm_DataHoraTimer(Sender: TObject);
    procedure ac_postosExecute(Sender: TObject);
    procedure ac_combustiveisExecute(Sender: TObject);
    procedure ac_bombasExecute(Sender: TObject);
    procedure ac_tanquesExecute(Sender: TObject);
    procedure ac_abastecimentoExecute(Sender: TObject);
    procedure rel_abastecimentoExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  admPrincipal: TadmPrincipal;

implementation
  uses
    u_funcoes, u_movAbastecimento;

{$R *.dfm}

procedure TadmPrincipal.ac_abastecimentoExecute(Sender: TObject);
begin
  TmovAbastecimento.inicializa;
end;

procedure TadmPrincipal.ac_bombasExecute(Sender: TObject);
begin
  AbriFormularios('cadBombas', 1);
end;

procedure TadmPrincipal.ac_combustiveisExecute(Sender: TObject);
begin
  AbriFormularios('cadCombustivel', 1);
end;

procedure TadmPrincipal.ac_postosExecute(Sender: TObject);
begin
  AbriFormularios('cadPostos', 1);
end;

procedure TadmPrincipal.ac_tanquesExecute(Sender: TObject);
begin
  AbriFormularios('cadTanques', 1);
end;

procedure TadmPrincipal.rel_abastecimentoExecute(Sender: TObject);
begin
  TRelatorios.inicializa;
end;

procedure TadmPrincipal.tm_DataHoraTimer(Sender: TObject);
begin
  Statusbar1.Panels [1].Text := ' '+FormatDateTime ('dddd", "dd" de "mmmm" de "yyyy',now);// para data
  statusbar1.Panels [3].Text := ' '+FormatDateTime ('hh:mm:ss',now);
end;

end.
