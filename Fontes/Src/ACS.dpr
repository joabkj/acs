program ACS;

uses
  Vcl.Forms,
  u_AdmPrincipal in 'u_AdmPrincipal.pas' {admPrincipal},
  u_cadBasico in 'u_cadBasico.pas' {cadBasico},
  Vcl.Themes,
  Vcl.Styles,
  u_dicionarioDados in 'u_dicionarioDados.pas',
  u_funcoes in 'u_funcoes.pas',
  u_connection in 'u_connection.pas',
  u_configBanco in 'u_configBanco.pas',
  u_cadCombustivel in 'u_cadCombustivel.pas' {cadCombustivel},
  u_cadTanques in 'u_cadTanques.pas' {cadTanques},
  u_PersistentObject in 'u_PersistentObject.pas',
  u_Atributo in 'u_Atributo.pas',
  u_Bombas_Combustiveis in 'u_Bombas_Combustiveis.pas',
  u_movAbastecimento in 'u_movAbastecimento.pas' {movAbastecimento},
  u_cadBombas in 'u_cadBombas.pas' {cadBombas},
  u_Abastecimento in 'u_Abastecimento.pas',
  u_dmDados in 'u_dmDados.pas' {Dados: TDataModule},
  u_basRelatorio in 'u_basRelatorio.pas' {Relatorios},
  u_cadPostos in 'u_cadPostos.pas' {cadPostos};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Lavender Classico');
  Application.CreateForm(TadmPrincipal, admPrincipal);
  Application.CreateForm(TDados, Dados);
  Application.CreateForm(TcadPostos, cadPostos);
  Application.Run;
end.
