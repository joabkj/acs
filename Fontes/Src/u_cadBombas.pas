unit u_cadBombas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, u_cadBasico, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, System.ImageList,
  Vcl.ImgList, Vcl.Menus, Vcl.Mask, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TcadBombas = class(TcadBasico)
    qy_dadosID_BOMBAS: TIntegerField;
    qy_dadosDESCRICAO: TWideStringField;
    function valida: Boolean; override;
    function pesq_complementar: String; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  cadBombas: TcadBombas;

implementation

{$R *.dfm}

function TcadBombas.pesq_complementar: String;
begin
  result:= '';
end;

function TcadBombas.valida: Boolean;
begin
  result:= True;
  if trim(db_nome.Text) = '' then
  begin
    Showmessage('Nome e obrigatório.');
    result:= False;
    db_nome.SetFocus;
    exit;
  end;
end;

initialization
    RegisterClass(TcadBombas);

end.
