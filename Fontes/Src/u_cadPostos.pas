unit u_cadPostos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, u_cadBasico, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, System.ImageList,
  Vcl.ImgList, Vcl.Menus, Vcl.Mask, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TcadPostos = class(TcadBasico)
    Label8: TLabel;
    db_cnpj: TDBEdit;
    db_telefone: TDBEdit;
    Label9: TLabel;
    qy_dadosID_POSTO: TIntegerField;
    qy_dadosDESCRICAO: TWideStringField;
    qy_dadosCNPJ: TWideStringField;
    qy_dadosTELEFONE: TWideStringField;
    function valida: Boolean; override;
    function pesq_complementar: String; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  cadPostos: TcadPostos;

implementation

{$R *.dfm}

{ TcadBasico1 }

function TcadPostos.pesq_complementar: String;
begin
  result:= '';
end;

function TcadPostos.valida: Boolean;
begin
  result:= true;
  if trim(db_nome.Text) = '' then
  begin
    Showmessage('Nome e obrigatório.');
    result:= False;
    db_nome.SetFocus;
    exit;
  end;
  if trim(db_cnpj.Text) = '' then
  begin
    Showmessage('CNPJ e obrigatório.');
    result:= False;
    db_cnpj.SetFocus;
    exit;
  end;
end;

initialization
    RegisterClass(TcadPostos);

end.
