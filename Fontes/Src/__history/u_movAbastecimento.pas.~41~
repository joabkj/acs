unit u_movAbastecimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Imaging.pngimage;

type
  TmovAbastecimento = class(TForm)
    pnl_top: TPanel;
    lbl_descr: TLabel;
    img_logo: TImage;
    Label1: TLabel;
    db_posto: TDBLookupComboBox;
    Label2: TLabel;
    db_bomba: TDBLookupComboBox;
    Label3: TLabel;
    db_combustivel: TDBLookupComboBox;
    edt_litros: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edt_valor_abastecido: TEdit;
    lbl_data: TLabel;
    Label6: TLabel;
    edt_valorTotal: TEdit;
    btn_salvar: TButton;
    btn_limpar: TButton;
    lbl_informa: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_limparClick(Sender: TObject);
    procedure edt_valor_abastecidoExit(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
    procedure edt_valorTotalExit(Sender: TObject);
    procedure edt_valor_abastecidoEnter(Sender: TObject);
  private
    { Private declarations }
    wp_valorImposto: Currency;
    procedure limpar;
    procedure limparParcal;
    function CalculaImposto(Avalor: Currency; AMargen: Currency): Currency;
    procedure DescricaoImposto;
    function valida: boolean;
  public
    { Public declarations }
    class function inicializa: boolean;
  end;

var
  movAbastecimento: TmovAbastecimento;

implementation

  uses u_dmDados, u_abastecimento;
{$R *.dfm}

procedure TmovAbastecimento.btn_limparClick(Sender: TObject);
begin
  limpar;
end;

procedure TmovAbastecimento.btn_salvarClick(Sender: TObject);
begin
  if valida then
  begin
    with TAbastecimento.Create do
    try
      DATA      := FormatDateTime('dd/mm/yyyy',now);
      HORA      := FormatDateTime('hh:mm',now);
      ID_TANQUES:= dados.qy_combustivelID_TANQUES.asinteger;
      ID_POSTO  := dados.qy_postosID_POSTO.asinteger;
      ID_COMBUSTIVEL := dados.qy_combustivelID_COMBUSTIVEIS.asinteger;
      ID_BOMBAS := dados.qy_bombasID_BOMBAS.asinteger;
      VALOR     := StrToFloat(edt_valor_abastecido.Text);
      VALOR_IMPOSTO:= wp_valorImposto;
      VALOR_TOTAL  := VALOR + VALOR_IMPOSTO;
      LITROS    := StrToInt(edt_litros.Text);
      if Insert then
      begin
        ShowMessage('Abastecimento Realizado com Sucesso!');
        limparParcal;
      end;
    finally
      Free;
    end;
  end;
end;

function TmovAbastecimento.CalculaImposto(Avalor: Currency; AMargen: Currency): Currency;
begin
  result:= Avalor * AMargen / 100;
end;

procedure TmovAbastecimento.DescricaoImposto;
begin
  lbl_informa.Caption:= 'Valor total de imposto cobrado R$: '+ FormatFloat(',0.00', wp_valorImposto);
  lbl_informa.Visible:= true;
end;

procedure TmovAbastecimento.edt_valorTotalExit(Sender: TObject);
begin
  TEdit(Sender).Text:=FormatFloat('###,###,##0.00',StrToFloat(TEdit(Sender).Text));
end;

procedure TmovAbastecimento.edt_valor_abastecidoEnter(Sender: TObject);
begin
  wp_valorImposto:= 0;
end;

procedure TmovAbastecimento.edt_valor_abastecidoExit(Sender: TObject);
begin
  if Trim(edt_valor_abastecido.Text) <> '' then
  begin
    TEdit(Sender).Text:=FormatFloat('###,###,##0.00',StrToFloat(TEdit(Sender).Text));
    wp_valorImposto    := CalculaImposto(StrToFloat(edt_valor_abastecido.Text), 13);
    edt_valorTotal.Text:= FloatToStr(wp_valorImposto + StrToFloat(edt_valor_abastecido.Text));
    DescricaoImposto;
  end;
end;

procedure TmovAbastecimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  movAbastecimento := Nil ;
  action:= cafree;
end;

procedure TmovAbastecimento.FormCreate(Sender: TObject);
begin
  lbl_data.Caption:= FormatDateTime ('dddd", "dd" de "mmmm" de "yyyy',now);
  dados.refresh;
end;

class function TmovAbastecimento.inicializa: boolean;
var
  FrmClass : TFormClass;
  Frm      : TForm;
begin
  try
    FrmClass   := TFormClass(FindClass('TmovAbastecimento'));
    Frm        := FrmClass.Create(Application);
    Frm.ShowModal;
  finally
    Frm.Free;
    result:= true;
  end;
end;

procedure TmovAbastecimento.limpar;
begin
  dados.fechar;
  db_posto.KeyValue := 0;
  db_bomba.KeyValue := 0;
  db_combustivel.KeyValue := 0;
  edt_litros.Clear;
  edt_valor_abastecido.Clear;
  edt_valorTotal.Clear;
  lbl_informa.Caption:= '';
  dados.Abrir;
end;

procedure TmovAbastecimento.limparParcal;
begin
  db_bomba.KeyValue := 0;
  db_combustivel.KeyValue := 0;
  edt_litros.Clear;
  edt_valor_abastecido.Clear;
  edt_valorTotal.Clear;
  lbl_informa.Caption:= '';
  lbl_informa.Visible:= not lbl_informa.Visible;
end;

function TmovAbastecimento.valida: boolean;
begin
  result:= True;
  if (db_posto.KeyValue = 0) or (db_posto.KeyValue = null)  then
  begin
    Showmessage('Posto e obrigatório.');
    result:= False;
    db_posto.SetFocus;
    exit;
  end;
  if (db_bomba.KeyValue = 0) or (db_bomba.KeyValue = null) then
  begin
    Showmessage('Bomba e obrigatório.');
    result:= False;
    db_bomba.SetFocus;
    exit;
  end;
  if (db_combustivel.KeyValue = 0) or (db_combustivel.KeyValue = null) then
  begin
    Showmessage('Combustivel e obrigatório.');
    result:= False;
    db_bomba.SetFocus;
    exit;
  end;
  if trim(edt_litros.Text) = '' then
  begin
    Showmessage('Informe a quantidade de litros.');
    result:= False;
    edt_litros.SetFocus;
    exit;
  end;
  if trim(edt_valor_abastecido.Text) = '' then
  begin
    Showmessage('Informe o valor do abastecimento.');
    result:= False;
    edt_valor_abastecido.SetFocus;
    exit;
  end;
end;

initialization
    RegisterClass(TmovAbastecimento);

end.
