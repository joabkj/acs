unit u_dicionarioDados;

interface
type
  TResultArray = array [1..5] of string;
  function DadosFormulario(p_tag: integer): TResultArray;

implementation

function DadosFormulario(p_tag: integer): TResultArray;
var
  Vet : TResultArray;
begin
  case p_tag of
    1 :
      begin
        Vet[1]:= 'POSTO';      //Nome da Tabela
        Vet[2]:= 'DESCRICAO'; // Campo Descri��o
        Vet[3]:= 'ID_POSTO';  // Campo Identificador
        Vet[4]:= 'true';      // Iniciar formulario com a query aberta
        vet[5]:= 'CADASTRO DE POSTOS'; // Nome do formulario
      end;
    2 :
      begin
        Vet[1]:= 'COMBUSTIVEIS';
        Vet[2]:= 'DESCRICAO';
        Vet[3]:= 'ID_COMBUSTIVEIS';
        Vet[4]:= 'true';
        vet[5]:= 'CADASTRO DE COMBUSTIVEIS';
      end;
    3 :
      begin
        Vet[1]:= 'TANQUES';
        Vet[2]:= 'DESCRICAO';
        Vet[3]:= 'ID_TANQUES';
        Vet[4]:= 'true';
        vet[5]:= 'CADASTRO DE TANQUES';
      end;
    4 :
      begin
        Vet[1]:= 'BOMBAS';
        Vet[2]:= 'DESCRICAO';
        Vet[3]:= 'ID_BOMBAS';
        Vet[4]:= 'true';
        vet[5]:= 'CADASTRO DE BOMBAS';
      end;
  end;




  result:= Vet;
end;

end.
