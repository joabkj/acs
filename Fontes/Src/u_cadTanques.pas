unit u_cadTanques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, u_cadBasico, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Actions, Vcl.ActnList,
  System.ImageList, Vcl.ImgList, Vcl.Menus, Vcl.Mask, Vcl.DBCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, u_Bombas_Combustiveis, System.UITypes;

type
  TcadTanques = class(TcadBasico)
    lock_posto: TFDQuery;
    qy_dadosDESCRICAO: TWideStringField;
    qy_dadosID_POSTO: TIntegerField;
    lock_postoID_POSTO: TIntegerField;
    lock_postoDESCRICAO: TWideStringField;
    qy_dadosPOSTO_DESCRICAO: TStringField;
    db_posto: TDBLookupComboBox;
    Label8: TLabel;
    gb_combustiveis: TGroupBox;
    qy_bombas: TFDQuery;
    ds_bombas: TDataSource;
    DBGrid1: TDBGrid;
    lock_combustiveis: TFDQuery;
    lock_combustiveisID_COMBUSTIVEIS: TIntegerField;
    lock_combustiveisDESCRICAO: TWideStringField;
    Label9: TLabel;
    db_bomba: TDBLookupComboBox;
    btn_incluir: TButton;
    btn_salvar: TButton;
    Button3: TButton;
    Label10: TLabel;
    db_combustivel: TDBLookupComboBox;
    FDQuery1: TFDQuery;
    qy_dadosID_TANQUES: TIntegerField;
    qy_dadosID_COMBUSTIVEIS: TIntegerField;
    qy_dadosCOMBUSTIVEL_DESCRICAO: TStringField;
    qy_bombasID_TANQUES_BOMBAS: TIntegerField;
    qy_bombasID_TANQUES: TIntegerField;
    qy_bombasID_BOMBAS: TIntegerField;
    lock_bombas: TFDQuery;
    lock_bombasID_BOMBAS: TIntegerField;
    lock_bombasDESCRICAO: TWideStringField;
    qy_bombasBOMBAS_DESCRICAO: TStringField;
    procedure FormShow(Sender: TObject);
    procedure btn_incluirClick(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
    function valida: Boolean; override;
    function pesq_complementar: String; override;
    procedure ac_ExcluirExecute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    FBombas: TBombas_Combustiveis;
    procedure IncluiItem;
  public
    { Public declarations }
  end;

var
  cadTanques: TcadTanques;

implementation

{$R *.dfm}

procedure TcadTanques.ac_ExcluirExecute(Sender: TObject);
var
  wl_id: String;
begin
  wl_id:= qy_dadosID_TANQUES.AsString;

  if btn_excluir.Visible then
  begin
    if not qy_dados.IsEmpty then
    begin
      if messagedlg('Deseja excluir este registro?', mtconfirmation,[mbyes,mbno],0)= idyes then
      begin
        try
          if not Assigned(FBombas) then
            FBombas := TBombas_Combustiveis.Create;
          FBombas.Load(StrToIntDef(wl_id,0));
          with FBombas do
          begin
            if Delete then
            begin
              qy_dados.Delete;
              Showmessage('Itens Deletados com sucesso.');
            end;
          end;
        except
          Showmessage('Erro ao excluir o registro. Provalvelmente existe tabelas com registros relacionados.');
        end;
      end;
    end
    else
      begin
        Showmessage('N�o existe registro para ser excluido.');
        exit;
      end;
  end;
end;

procedure TcadTanques.btn_incluirClick(Sender: TObject);
begin
  inherited;
  if not (qy_bombas.State in [dsInsert]) then
  begin
    if qy_dadosID_TANQUES.AsInteger > 0 then
      IncluiItem
    else if qy_dados.State in [dsInsert] then
    begin
      if messagedlg('Deseja salvar este registro para continuar cadastrando os itens ?', mtconfirmation,[mbyes,mbno],0)= idyes then
      begin
        qy_dados.post;
        qy_dados.edit;
        IncluiItem;
      end;
    end;
  end
  else
    Showmessage('Voc� j� estar incluindo um registro. Salve para incluir um novo.');
end;

procedure TcadTanques.btn_salvarClick(Sender: TObject);
begin
  inherited;
  qy_bombas.Post;
  db_bomba.Enabled:= false;
end;

procedure TcadTanques.Button3Click(Sender: TObject);
begin
  inherited;
  if (not qy_bombas.IsEmpty) or not (qy_bombas.State in [dsInsert]) then
    qy_bombas.delete;
end;

procedure TcadTanques.FormShow(Sender: TObject);
begin
  qy_bombas.Connection         := qy_dados.Connection;
  lock_posto.Connection        := qy_dados.Connection;
  lock_bombas.Connection       := qy_dados.Connection;
  lock_combustiveis.Connection := qy_dados.Connection;
  qy_bombas.Open;
  inherited;
end;

procedure TcadTanques.IncluiItem;
begin
  qy_bombas.Insert;
  qy_bombasID_TANQUES.AsInteger:= qy_dadosID_TANQUES.AsInteger;
  db_bomba.Enabled:= true;
  btn_salvar.Enabled:= true;
  db_bomba.SetFocus;
end;

function TcadTanques.pesq_complementar: String;
begin
  result:= '';
end;

function TcadTanques.valida: Boolean;
begin
  result:= True;
  if trim(db_nome.Text) = '' then
  begin
    Showmessage('Nome e obrigat�rio.');
    result:= False;
    db_nome.SetFocus;
    exit;
  end;
  if trim(db_posto.Text) = '' then
  begin
    Showmessage('Posto e obrigat�rio.');
    result:= False;
    db_posto.SetFocus;
    exit;
  end;
  if trim(db_combustivel.Text) = '' then
  begin
    Showmessage('Combustivel e obrigat�rio.');
    result:= False;
    db_combustivel.SetFocus;
    exit;
  end;

end;

initialization
    RegisterClass(TcadTanques);

end.
